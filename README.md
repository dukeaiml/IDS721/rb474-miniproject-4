# rb474-miniproject-4

Containerization of a simple Rust Actix web app

## Process
1. Follow the steps in [the guide](https://actix.rs/docs/getting-started) to create a simple "Hello, world!" app
2. Change the IP in the last code snippet from "127.0.0.1" to "0.0.0.0"
3. `docker init` in the app's directory.
4. Prompted by the Docker, answer:
- What application platform does your project use?: **Rust**
- What version of Rust do you want to use?: choose the **default** one, for me it was 1.71.0
- What port does your server listen on?: **8080**
5. `docker compose up --build` will build and run the container. You can access the application at http://localhost:8080, where you will see "Hello, world!" or "Hey there!" at http://127.0.0.1:8080/hey.

The image's size is reasonably small (around 90MB).

## Result
### Running container
![](images/running_container.png)

### Docker Desktop
![](images/docker_desktop.png)

### In a browser
![](images/in_browser.png)